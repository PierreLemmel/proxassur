$(function() {
    let deferredPrompt;

    const installPromotion = $("#install-promotion");
    function showInstallPromotion() {
        installPromotion.show();
    }

    function hideInstallPromotion() {
        installPromotion.hide();
    }

    window.addEventListener('beforeinstallprompt', (e) => {

        console.log(`'beforeinstallprompt'`);
        e.preventDefault();
        deferredPrompt = e;
        
        showInstallPromotion();
        
        console.log(`'beforeinstallprompt'`);
    });

    const installBtn = $('#install-btn');
    installBtn.click(async () => {

        hideInstallPromotion();

        if (!deferredPrompt) {
            console.error("Can't find a deferred prompt");
            return;
        }

        deferredPrompt.prompt();

        const { outcome } = await deferredPrompt.userChoice;

        console.log(`User response to the install prompt: ${outcome}`);

        deferredPrompt = null;
      });

    window.addEventListener('appinstalled', () => {

        hideInstallPromotion();
        deferredPrompt = null;

        console.log('PWA was installed');
    });

    function getPWADisplayMode() {

        if (document.referrer.startsWith('android-app://')) {
          return 'twa';
        } else if (navigator.standalone || window.matchMedia('(display-mode: standalone)').matches) {
          return 'standalone';
        }
        return 'browser';
    }

    hideInstallPromotion();
});